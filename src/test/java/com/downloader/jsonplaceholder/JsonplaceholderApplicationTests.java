package com.downloader.jsonplaceholder;

import com.downloader.jsonplaceholder.service.DataService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
class JsonplaceholderApplicationTests {

    private static final Logger LOG = LoggerFactory.getLogger(JsonplaceholderApplicationTests.class);
    private static final String ID_KEY = "id";
    private static final String TITLE_KEY = "title";
    private static final String BODY_KEY = "body";
    private static final String USER_ID_KEY = "userId";

    @SpyBean
    private DataService dataService;

    @Value("${file.saving.directory}")
    private String savingDirectory;

    @Value("${file.format.json}")
    private String fileFormat;

    @AfterEach
    void clean() {
        cleanFilesDirectory();
    }

    @Test
    void shouldSaveObjectsArrayToSeparateFiles() {
        //given
        int objectsQuantity = 5;
        prepareForTest(objectsQuantity);

        //when
        dataService.savePostsToFiles();
        List<File> createdFilesList = getFilesInDirectory();

        //then
        assertEquals(objectsQuantity, createdFilesList.size());
        assertTrue(containsAllFilesInDirectory(createdFilesList, objectsQuantity));
    }

    @Test
    void fileShouldContainsAllJsonFields() throws IOException {
        //given
        prepareForTest(1);
        String fileObject = StringUtils.EMPTY;

        //when
        dataService.savePostsToFiles();
        List<File> createdFilesList = getFilesInDirectory();
        if (CollectionUtils.isNotEmpty(createdFilesList)) {
            fileObject = FileUtils.readFileToString(createdFilesList.get(0));
        }

        assertTrue(fileObject.contains(ID_KEY));
        assertTrue(fileObject.contains(BODY_KEY));
        assertTrue(fileObject.contains(TITLE_KEY));
        assertTrue(fileObject.contains(USER_ID_KEY));
    }

    private void prepareForTest(int objectsQuantity) {
        JSONArray jsonObjects = prepareJsonObjectsArray(objectsQuantity);
        when(dataService.readJsonFromUrl()).thenReturn(Optional.of(jsonObjects));
    }

    private JSONArray prepareJsonObjectsArray(int objectsQuantity) {
        JSONArray array = new JSONArray();
        for (int i = 1; i <= objectsQuantity; i++) {
            createObject(i).ifPresent(array::put);
        }
        return array;
    }

    private Optional<JSONObject> createObject(int objectNumber) {
        try {
            JSONObject object = new JSONObject();
            object.put(ID_KEY, objectNumber);
            object.put(TITLE_KEY, TITLE_KEY + objectNumber);
            object.put(BODY_KEY, BODY_KEY + objectNumber);
            object.put(USER_ID_KEY, USER_ID_KEY + objectNumber);
            return Optional.of(object);
        } catch (JSONException e) {
            LOG.error("Unable to create object for number {}!", objectNumber, e);
        }
        return Optional.empty();
    }

    private List<File> getFilesInDirectory() {
        return Optional.of(new File(savingDirectory))
                .map(File::listFiles)
                .map(Arrays::asList)
                .orElse(Collections.emptyList());
    }

    private boolean containsAllFilesInDirectory(List<File> files, int objectsQuantity) {
        List<String> fileNames = getFilesName(files);
        for (int i = 1; i <= objectsQuantity; i++) {
            if (!fileNames.contains(i + fileFormat)) {
                LOG.error("File {} doesn't exist!", i + fileFormat);
                return false;
            }
        }
        return true;
    }

    private List<String> getFilesName(List<File> files) {
        return files.stream()
                .map(File::getName)
                .collect(Collectors.toList());
    }

    private void cleanFilesDirectory() {
        try {
            FileUtils.cleanDirectory(new File(savingDirectory));
        } catch (IOException e) {
            LOG.error("Unable to clean directory {}", savingDirectory, e);
        }
    }
}
