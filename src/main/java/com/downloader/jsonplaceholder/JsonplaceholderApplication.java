package com.downloader.jsonplaceholder;

import com.downloader.jsonplaceholder.service.DataService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class JsonplaceholderApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(JsonplaceholderApplication.class, args);
        DataService dataService = ctx.getBean(DataService.class);
        dataService.savePostsToFiles();
        ctx.close();
    }

}
