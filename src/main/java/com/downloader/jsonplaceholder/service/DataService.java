package com.downloader.jsonplaceholder.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

@Service
@Profile("dev")
public class DataService {

    private static final Logger LOG = LoggerFactory.getLogger(DataService.class);
    private static final String JSON_VALUE = "id";

    @Value("${jsonplaceholder.url}")
    private String url;

    @Value("${file.format.json}")
    private String fileFormat;

    @Value("${file.saving.directory}")
    private String savingDirectory;

    public void savePostsToFiles() {
        LOG.info("Starting do read and save response from {}", url);
        Optional<JSONArray> jsonArray = readJsonFromUrl();
        jsonArray.ifPresent(this::writeArrayToFiles);
        LOG.info("Reading and saving response from {} to file completed", url);
    }

    public Optional<JSONArray> readJsonFromUrl() {
        try (InputStream is = new URL(url).openStream()) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            String jsonText = readAll(rd);
            return Optional.of(new JSONArray(jsonText));
        } catch (MalformedURLException e) {
            LOG.error("Unable to create URL object for {}", url, e);
        } catch (IOException e) {
            LOG.error("Unable to open stream or read data for response from {}", url, e);
        } catch (JSONException e) {
            LOG.error("Unable to convert data to JSON objects while reading for response from {}", url, e);
        }
        return Optional.empty();
    }

    private String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    private void writeArrayToFiles(JSONArray jsonArray) {
        createDirectoryIfNotExist();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                writeToFile(jsonArray.getJSONObject(i));
            } catch (JSONException e) {
                LOG.error("Unable to convert data to JSON objects while writing data to {}  file " +
                        "for response from {}", fileFormat, url, e);
            }
        }
    }

    private void writeToFile(JSONObject jsonObject) {
        try (FileWriter file = new FileWriter(savingDirectory + jsonObject.getString(JSON_VALUE) + fileFormat, true)) {
            file.write(String.valueOf(jsonObject));
            file.flush();
        } catch (JSONException e) {
            LOG.error("Unable to get {} value from JSON objects while writing object to file for response from  {}",
                    JSON_VALUE, url, e);
        } catch (IOException e) {
            LOG.error("Unable to write data for response from {} to file", url, e);
        }
    }

    private void createDirectoryIfNotExist() {
        File directory = new File(savingDirectory);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }
}

